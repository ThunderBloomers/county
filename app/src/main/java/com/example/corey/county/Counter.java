package com.example.corey.county;

import android.os.Bundle;

/**
 * Created by Corey on 5/3/2017.
 */

public class Counter {
    private int COUNT;

    public Counter(){
        super();
        setCount(0);
    }

    public void setCount(int num){
        COUNT = num;
    }

    public void countStepUp(){
        if (COUNT >= 0){
            COUNT++;
        }else{
            setCount(0);
            COUNT++;
        }
    }

    public void countStepDown(){
        if (COUNT > 0){
            COUNT++;
        }else{
            setCount(0);
        }
    }

    public int getCount(){
        return COUNT;
    }

    public CharSequence getCountString(){
        String countString = String.valueOf(COUNT);
        return countString;
    }

}
