package com.example.corey.county;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Counter BCount;
    private TextView label;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BCount = new Counter();
        label = (TextView)findViewById(R.id.myTextView);
        label.setText("No Current Count");
    }


    public void countStepUp(View view){
        if(BCount != null){
            BCount.countStepUp();
            updateTextLabel();
        }
    }

    public void updateTextLabel() {
        if (label != null) {
            label.setText(BCount.getCountString());
        }
    }
}


